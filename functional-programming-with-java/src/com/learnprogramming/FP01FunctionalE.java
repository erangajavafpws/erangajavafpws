package com.learnprogramming;

import java.util.List;

/*
   lesson 8 section 2.
   print only square of the even numbers.
   Uses map(). 
 */
public class FP01FunctionalE {
	public static void main(String[] args) {
		
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		printSquareOfEvenNumbers(numbers);
	}

	private static void printSquareOfEvenNumbers(List<Integer> numbers) {
		//what to do?		
		numbers.stream()			
			.filter(number -> number % 2 == 0) 
			.map(number -> number * number)
			.forEach(System.out::println); 
	}		
}
