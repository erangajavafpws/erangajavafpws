package com.learnprogramming;

import java.util.List;

/*
   lesson 6 section 2.
   print only even numbers.
   Uses filter() but filter uses a lambda. 
 */
public class FP01FunctionalD {
	public static void main(String[] args) {
		
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		printEvenNumbersInListFunctional(numbers);
	}

	private static void printEvenNumbersInListFunctional(List<Integer> numbers) {
		//what to do?		
		numbers.stream()			
			.filter(number -> number % 2 == 0) //filter: only allows even numbers.
			.forEach(System.out::println); //method reference
	}		
}
